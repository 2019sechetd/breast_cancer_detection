# Use the just command runner to execute recipes: https://github.com/casey/just

_default:
    @just --list

# Format codebase using black
fmt:
    python -m black breast_cancer_detection
    python -m isort breast_cancer_detection

# Lint codebase with pylint
lint:
    python -m pylint breast_cancer_detection