# Breast cancer detection

*Massinissa Beldjenna, Alix Chazottes, Ludovic Trautmann, Dylan Sechet, François Philippe*

This project uses a deep learning based architecture to detect tumors in mammograms.

## Installation
* Install python 3.10.
* Clone the repo: `git clone https://github.com/Seon82/breast_cancer_detection.git`
* Run `pip install .` from the project's root to install it along with any dependencies.

## Contributing

We use [poetry](https://python-poetry.org/) to manage dependencies. 
* Follow [the official instructions](https://python-poetry.org/docs/#installation) to install it.
* Run `poetry install` from the root of the project to install it along with its dependencies.

NB: You should use `poetry add numpy` and `poetry remove numpy` to install and uninstall dependencies.


